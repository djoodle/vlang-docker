FROM alpine:latest
LABEL maintainer="djoodle <daniel.udell@gmail.com>"

ENV V_HOME=/opt/vlang

# get dependencies
RUN apk --no-cache add \
  git make upx gcc \
  musl-dev \
  openssl-dev sqlite-dev \
  libx11-dev glfw-dev freetype-dev

# graphics dependencies
RUN apk --no-cache add --virtual sdl2deps sdl2-dev sdl2_ttf-dev sdl2_mixer-dev sdl2_image-dev

# clone and build v
RUN git clone https://github.com/vlang/v ${V_HOME} \
  && cd ${V_HOME} \
  && make \
  && ./v --version \
  && ./v symlink

# clone and build vpkg
RUN git clone https://github.com/vpkg-project/vpkg ${V_HOME}/vpkg \
  && cd ${V_HOME}/vpkg \
  && ${V_HOME}/v -prod . \
  && ./vpkg version \
  && ln -s ${V_HOME}/vpkg/vpkg /usr/local/bin/vpkg

WORKDIR /code/
ENTRYPOINT ["v"]
CMD ["--version"]  
